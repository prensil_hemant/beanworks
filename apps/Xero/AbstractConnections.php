<?php
/**
 * Abstract AbstractConnections Class.
 * Abstract class helps to add some base methods requires for all kind of Xero App.
 *
 * @author    Hemant Patel <Hemant@prensil.com>
 */

namespace Xero;

abstract class AbstractConnections
{
    // Implement in derived class
    abstract public function createXeroApp();
}
