<?php
/**
 * XeroVendorServices Class.
 * Service class helps to fetch vendors/contacts from Xero.
 * It also helps to store them in database for further processing.
 *
 * @author    Hemant Patel <Hemant@prensil.com>
 */

namespace Services;

use Xero\XeroConnection;
use XeroPHP\Models\Accounting\Contact;
use Models\VendorModel;

class XeroVendorServices extends BaseService
{
    /**
     * Class variables.
     */
    public $vendors;
    private $api_data;
    public $status;

    /**
     * Initialize class properties.
     */
    public function __construct($config)
    {
        // Request appropriate App object from Factory class.
        $this->app = (new XeroConnection($config))->createXeroApp();
        $this->vendors = [];
        $this->api_data = null;
        $this->response['success'] = 1;
        // Xero Active Contact status
        $this->status = Contact::CONTACT_STATUS_ACTIVE;
    }

    /**
     * Fetch vendors/contact from Xero app. Check them if they already exist in DB then
     * update account details or create new vendors/contacts.
     *
     * @return array
     */
    public function saveVendors()
    {
        $this->fetchVendors();
        if (count($this->vendors) > 0) {
            foreach ($this->vendors as $Vendor) {
                // VendorID should always be unique.
                if (isset($Vendor['VendorID']) && $Vendor['VendorID'] != '') {
                    $Vendor_model = VendorModel::byId($Vendor['VendorID']);
                }
                if (is_null($Vendor_model)) {
                    $Vendor_model = new VendorModel();
                }
                if (!$Vendor_model->saveVendor($Vendor)) {
                    $this->response['success'] = false;
                    $this->response['errors'] = $account_model->errors;
                }
            }
        }

        return $this->response;
    }

    /**
     * Pull vendors details based on where clauses and get an api object.
     * Send that object to private method for more processing.
     *
     * @param object
     *
     * @return array
     */
    private function fetchVendors()
    {
        try {
            $this->api_data = $this->app->load(Contact::class)->where('ContactStatus', $this->status)->execute();
            $i = count($this->vendors);
            if (!is_null($this->api_data) && count($this->api_data) > 0) {
                foreach ($this->api_data as $object) {
                    $this->vendors[$i++] = $this->setVendorAttributes($object);
                }
            }
        } catch (\Exception $e) {
            // Error occured during API call.
            $this->response['errors'] = $this->getErrorMessages($e);
            $this->response['success'] = false;
        }
    }

    /**
     * Fetch all the necessary details for an vendor/contact from the given API object.
     * Convert it to an array and return it for more local processing.
     *
     * @param object
     *
     * @return array
     */
    private function setVendorAttributes($object)
    {
        $Vendor['VendorID'] = $object['ContactID'];
        $Vendor['Name'] = $object['Name'];
        $Vendor['TaxNumber'] = $object['TaxNumber'];
        $Vendor['FirstName'] = $object['FirstName'];
        $Vendor['LastName'] = $object['LastName'];
        $Vendor['EmailAddress'] = $object['EmailAddress'];
        $Vendor['SkypeUserName'] = $object['SkypeUserName'];
        $Vendor['IsSupplier'] = $object['IsSupplier'];
        $Vendor['IsCustomer'] = $object['IsCustomer'];
        $Vendor['XeroNetworkKey'] = $object['XeroNetworkKey'];
        $Vendor['Website'] = $object['Website'];
        $Vendor['Status'] = $this->status;

        return $Vendor;
    }
}
