<?php
/**
 * XeroAccountServices Class.
 * Service class helps to fetch accounts from Xero.
 * It also helps to store them in database for further processing.
 *
 * @author    Hemant Patel <Hemant@prensil.com>
 */

namespace Services;

use Xero\XeroConnection;
use XeroPHP\Models\Accounting\Account;
use Models\AccountModel;

class XeroAccountServices extends BaseService
{
    /**
     * Class variables.
     */
    public $accounts;
    private $api_data;
    private $response;
    public $payment_enabled;
    public $status;

    /**
     * Initialize class properties.
     */
    public function __construct($config)
    {
        // Request appropriate App object from Factory class.
        $this->app = (new XeroConnection($config))->createXeroApp();
        $this->accounts = [];
        $this->api_data = null;
        $this->response['success'] = 1;
        $this->payment_enabled = true;
        // Xero Active Account status
        $this->status = Account::ACCOUNT_STATUS_ACTIVE;
    }

    /**
     * Fetch accounts from Xero app. Check them if they already exist in DB then
     * update account details or create new accounts.
     *
     * @return array
     */
    public function saveAccounts()
    {
        $this->fetchAccounts();
        if (count($this->accounts) > 0) {
            foreach ($this->accounts as $account) {
                // Account ID should always be unique.
                if (isset($account['AccountID']) && $account['AccountID'] != '') {
                    $account_model = AccountModel::byId($account['AccountID']);
                }
                if (is_null($account_model)) {
                    $account_model = new AccountModel();
                }
                if (!$account_model->saveAccount($account)) {
                    $this->response['success'] = false;
                    $this->response['errors'] = $account_model->errors;
                }
            }
        }

        return $this->response;
    }

    /**
     * Pull account details based on where clauses and get an api object.
     * Send that object to private method for more processing.
     *
     * @param object
     *
     * @return array
     */
    private function fetchAccounts()
    {
        try {
            $this->api_data = $this->app->load(Account::class)->where('Status', $this->status)->where('EnablePaymentsToAccount', $this->payment_enabled)->execute();
            $i = count($this->accounts);
            if (!is_null($this->api_data) && count($this->api_data) > 0) {
                foreach ($this->api_data as $object) {
                    $this->accounts[$i++] = $this->setAccountAttributes($object);
                }
            }
        } catch (\Exception $e) {
            // Error occured during API call.
            $this->response['errors'] = $this->getErrorMessages($e);
            $this->response['success'] = false;
        }
    }

    /**
     * Fetch all the necessary details for an account from the given API object.
     * Convert it to an array and return it for more local processing.
     *
     * @param object
     *
     * @return array
     */
    private function setAccountAttributes($object)
    {
        $account['AccountID'] = $object['AccountID'];
        $account['Name'] = $object['Name'];
        $account['Type'] = $object['Type'];
        $account['Description'] = $object['Description'];
        $account['TaxType'] = $object['TaxType'];
        $account['BankAccountType'] = $object['BankAccountType'];
        $account['CurrencyCode'] = $object['CurrencyCode'];
        $account['ReportingCodeName'] = $object['ReportingCodeName'];
        $account['EnablePaymentsToAccount'] = $object['EnablePaymentsToAccount'];
        $account['ShowInExpenseClaims'] = $object['ShowInExpenseClaims'];
        $account['Status'] = $this->status;
        $account['updated_at'] = $object['UpdatedDateUTC']->format('Y-m-d H:i:s');

        return $account;
    }
}
