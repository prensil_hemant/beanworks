<?php
/**
 * XeroInvoiceServices Class.
 * Service class helps to fetch invoices from Xero.
 * It also helps to store them in database for further processing.
 *
 * @author    Hemant Patel <Hemant@prensil.com>
 */

namespace Services;

use Xero\XeroConnection;
use XeroPHP\Models\Accounting\Invoice;
use Models\InvoiceModel;

class XeroInvoiceServices extends BaseService
{
    /**
     * Class variables.
     */
    public $invoices;
    private $api_data;
    public $status;
    private $response;
    public $type;

    /**
     * Initialize class properties.
     */
    public function __construct($config)
    {
        // Request appropriate App object from Factory class.
        $this->app = (new XeroConnection($config))->createXeroApp();
        $this->invoices = [];
        $this->response['success'] = 1;
        $this->api_data = null;
        // Xero AUTHORISED Invoice status
        $this->status = Invoice::INVOICE_STATUS_AUTHORISED;
        // Fetch only Account payable invoices(Bills)
        $this->type = Invoice::INVOICE_TYPE_ACCPAY;
    }

    /**
     * Fetch invoices from Xero app. Check them if they already exist in DB then
     * update account details or create new invoices.
     *
     * @return array
     */
    public function saveInvoices()
    {
        $this->fetchInvoices();
        if (count($this->invoices) > 0) {
            foreach ($this->invoices as $invoice) {
                // InvoiceID should always be unique.
                if (isset($invoice['InvoiceID']) && $invoice['InvoiceID'] != '') {
                    $invoice_model = InvoiceModel::byId($invoice['InvoiceID']);
                }
                if (is_null($invoice_model)) {
                    $invoice_model = new InvoiceModel();
                }
                if (!$invoice_model->saveInvoice($invoice)) {
                    $this->response['success'] = false;
                    $this->response['errors'] = $invoice_model->errors;
                }
            }
        }

        return $this->response;
    }

    /**
     * Pull invoice details based on where clauses and get an api object.
     * Send that object to private method for more processing.
     *
     * @param object
     *
     * @return array
     */
    private function fetchInvoices()
    {
        try {
            $this->api_data = $this->app->load(Invoice::class)->where('Status', $this->status)->where('Type', $this->type)->execute();
            $i = count($this->invoices);
            if (!is_null($this->api_data) && count($this->api_data) > 0) {
                foreach ($this->api_data as $object) {
                    $this->invoices[$i++] = $this->setInvoiceAttributes($object);
                }
            }
        } catch (\Exception $e) {
            // Error occured during API call.
            $this->response['errors'] = $this->getErrorMessages($e);
            $this->response['success'] = false;
        }
    }

    /**
     * Fetch all the necessary details for an invoice from the given API object.
     * Convert it to an array and return it for more local processing.
     *
     * @param object
     *
     * @return array
     */
    private function setInvoiceAttributes($object)
    {
        $invoice['InvoiceID'] = $object['InvoiceID'];
        $invoice['InvoiceNumber'] = $object['InvoiceNumber'];
        $invoice['VendorID'] = $object['Contact']['ContactID'];
        $invoice['invoice_to'] = $object['Contact']['Name'];
        $invoice['CurrencyCode'] = $object['CurrencyCode'];
        $invoice['Status'] = $object['Status'];
        $invoice['SubTotal'] = $object['SubTotal'];
        $invoice['TotalTax'] = $object['TotalTax'];
        $invoice['Total'] = $object['Total'];
        $invoice['AmountDue'] = $object['AmountDue'];
        $invoice['AmountPaid'] = $object['AmountPaid'];
        $invoice['FullyPaidOnDate'] = $object['FullyPaidOnDate'];
        $invoice['AmountCredited'] = $object['AmountCredited'];
        $invoice['invoice_date'] = $object['Date']->format('Y-m-d H:i:s');
        $invoice['due_date'] = $object['DueDate']->format('Y-m-d H:i:s');

        return $invoice;
    }
}
