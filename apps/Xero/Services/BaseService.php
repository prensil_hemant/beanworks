<?php
/**
 * BaseService Class.
 * A base class for all Xero Services classes will add some base mmethods for all Services.
 *
 * @author    Hemant Patel <Hemant@prensil.com>
 */

namespace Services;

abstract class BaseService
{
    public $response_type = 'JSON';

    /**
     * Fetch and parse exception's error messages.
     *
     * @return mixed
     */
    protected function getErrorMessages($e)
    {
        return $e->getMessage();
    }

    /**
     * Send API response based on the requested response type. Default response type is JSON.
     *
     * @return mixed
     */
    protected function sendAPIResponse($result)
    {
        switch ($this->response_type) {
            case 'JSON':
                return json_encode($result);
        }
    }
}
