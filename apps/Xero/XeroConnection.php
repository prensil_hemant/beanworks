<?php
/**
 * XeroConnection Class.
 * A factory Class helps to connect with Xero with specified config settings and return an application object.
 *
 * @author    Hemant Patel <Hemant@prensil.com>
 */

namespace Xero;

use XeroPHP\Application\PrivateApplication;
use XeroPHP\Application\PartnerApplication;

class XeroConnection extends AbstractConnections
{
    /**
     * Initialize class properties.
     */
    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * Based on the app type defined in Config file, method will return an application object from API wrapper.
     *
     * @return object
     */
    public function createXeroApp()
    {
        try {
            switch ($this->config['app_type']) {
                // Xero App type as Private
                case 'PRIVATE':
                        return new PrivateApplication($this->config);
                // Xero App type as Public
                case 'PUBLIC':
                        return new PrivateApplication($this->config);
                // Xero App type as Partner
                case 'PARTNER':
                        return new PartnerApplication($this->config);
            }
        } catch (\Exception $e) {
            var_dump($e);
            die;
        }
    }
}
