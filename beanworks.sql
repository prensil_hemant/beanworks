-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 24, 2018 at 03:11 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `beanworks`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE IF NOT EXISTS `accounts` (
  `account_id` varchar(64) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `description` text,
  `type` int(32) NOT NULL,
  `tax_type` int(64) DEFAULT NULL,
  `bank_account_type` varchar(64) DEFAULT NULL,
  `currency_code` varchar(16) DEFAULT NULL,
  `reporting_code_name` varchar(16) DEFAULT NULL,
  `enable_payment_to_account` int(11) NOT NULL DEFAULT '0',
  `show_in_expense_claims` int(11) NOT NULL DEFAULT '0',
  `status` varchar(16) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE IF NOT EXISTS `invoices` (
  `invoice_id` varchar(64) NOT NULL,
  `vendor_id` varchar(64) NOT NULL,
  `invoice_no` varchar(64) DEFAULT NULL,
  `currency` varchar(16) NOT NULL,
  `invoice_status` varchar(16) NOT NULL,
  `subtotal` double NOT NULL,
  `total_tax` double NOT NULL,
  `total_amount` double NOT NULL,
  `amount_due` double NOT NULL,
  `amount_paid` double NOT NULL,
  `fully_paid_date` datetime DEFAULT NULL,
  `amount_credited` double NOT NULL,
  `invoice_date` datetime NOT NULL,
  `due_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE IF NOT EXISTS `vendors` (
  `vendor_id` varchar(64) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `tax_number` varchar(128) DEFAULT NULL,
  `first_name` varchar(128) DEFAULT NULL,
  `last_name` varchar(128) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `skype` varchar(256) DEFAULT NULL,
  `is_supplier` int(11) DEFAULT '0',
  `is_customer` int(11) DEFAULT '0',
  `network_key` int(64) DEFAULT NULL,
  `website` varchar(256) DEFAULT NULL,
  `status` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
 ADD PRIMARY KEY (`account_id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
 ADD PRIMARY KEY (`invoice_id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
 ADD PRIMARY KEY (`vendor_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
