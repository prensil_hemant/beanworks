<?php
/**
 * This method can be a cron job to fetch accounts at regular interval.
 * It can also be used to pull and store in DB them on demand.
 *
 * @return json
*/
require __DIR__.'/../vendor/autoload.php';
require __DIR__.'/../config.php';

use Services\XeroAccountServices;

$accountService = new XeroAccountServices($config);
$response = $accountService->saveAccounts();
echo json_encode($response); exit(1);
