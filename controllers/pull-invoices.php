<?php
/**
 * Pull invoices details from database and send a json response for UI rendering.
 *
 * @return json
*/
require __DIR__.'/../vendor/autoload.php';
require __DIR__.'/helpers.php';
use Models\InvoiceModel;

$request_body = file_get_contents('php://input');
$offset = 0;
$limit = 10;
$invoice_obj = InvoiceModel::where('1');
// Check request body and add filter and pagination params.
processFiltersAndPaginationParams($invoice_obj, $offset, $limit);
$response = [];
$response_data = [];
// Fetch data from Database
$invoices = $invoice_obj->withTotalCount()->get(array($offset, $limit));
if (count($invoices) > 0) {
    foreach ($invoices as $vendor) {
        array_push($response, $vendor->data);
    }
}
$response_data['resultData']['searchResults'] = $response;
$response_data['resultData']['totalAvailable'] = $invoice_obj->totalCount;
echo json_encode($response_data); exit(1);
