<?php
/**
 * Pull vendors details from database and send a json response for UI rendering.
 *
 * @return json
*/
require __DIR__.'/../vendor/autoload.php';
require __DIR__.'/helpers.php';
use Models\VendorModel;

$request_body = file_get_contents('php://input');
$offset = 0;
$limit = 10;
$vendor_obj = VendorModel::where('1');
// Check request body and add filter and pagination params.
processFiltersAndPaginationParams($vendor_obj, $offset, $limit);
$response = [];
$response_data = [];
// Fetch data from Database
$vendors = $vendor_obj->withTotalCount()->get(array($offset, $limit));
//$response_data['totalAvailable']=Vendor::count();
if (count($vendors) > 0) {
    foreach ($vendors as $vendor) {
        array_push($response, $vendor->data);
    }
}
$response_data['resultData']['searchResults'] = $response;
$response_data['resultData']['totalAvailable'] = $vendor_obj->totalCount;
echo json_encode($response_data); exit(1);
