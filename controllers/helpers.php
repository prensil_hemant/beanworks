<?php

// Process UI requests for filter or paginatoin.
function processFiltersAndPaginationParams(&$model_object, &$offset, &$limit)
{
    $request_body = file_get_contents('php://input');
    if (isset($request_body) && $request_body != '') {
        $data = json_decode($request_body, true);
        if (isset($data['filters']) && count($data['filters']) > 0) {
            foreach ($data['filters'] as $filter) {
                $account_obj = $model_object->where($filter['id'], '%'.$filter['value'].'%', 'LIKE');
            }
        }
        if (isset($data['pageNo']) && $data['pageNo'] != '') {
            $offset = $data['pageNo'] * $data['pageSize'] - $data['pageSize'];
        }
        if (isset($data['pageSize']) && $data['pageSize'] != '') {
            $limit = $data['pageSize'];
        }
    }
}
