<?php
/**
 * This method can be a cron job to fetch vendors at regular interval.
 * It can also be used to pull and store in DB them on demand.
 *
 * @return json
*/
require __DIR__.'/../vendor/autoload.php';
require __DIR__.'/../config.php';

use Services\XeroVendorServices;

$vendorService = new XeroVendorServices($config);
$response = $vendorService->saveVendors();
echo json_encode($response); exit(1);
