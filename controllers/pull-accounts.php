<?php
/**
 * Pull account details from database and send a json response for UI rendering.
 *
 * @return json
*/
require __DIR__.'/../vendor/autoload.php';
require __DIR__.'/helpers.php';
use Models\AccountModel;

// Start from the beginning
$offset = 0;
// Per page records
$limit = 10;
$account_obj = AccountModel::where('1');

// Check request body and add filter and pagination params.
processFiltersAndPaginationParams($account_obj, $offset, $limit);
$response = [];
$response_data = [];
// Fetch data from Database
$accounts = $account_obj->withTotalCount()->get(array($offset, $limit));
if (count($accounts) > 0) {
    foreach ($accounts as $account) {
        array_push($response, $account->data);
    }
}
$response_data['resultData']['searchResults'] = $response;
$response_data['resultData']['totalAvailable'] = $account_obj->totalCount;
echo json_encode($response_data); exit(1);
