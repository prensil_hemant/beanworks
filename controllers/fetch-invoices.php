<?php
/**
 * This method can be a cron job to fetch invoices at regular interval.
 * It can also be used to pull and store in DB them on demand.
 *
 * @return json
*/
require __DIR__.'/../vendor/autoload.php';
require __DIR__.'/../config.php';

use Services\XeroInvoiceServices;

$invoiceService = new XeroInvoiceServices($config);
$response = $invoiceService->saveInvoices();
echo json_encode($response); exit(1);
