<?php
/**
 * XeroAccountImportTest Class.
 * Test cases for account import functionality. Check successful insert and update if exist already.
 *
 * @author    Hemant Patel <Hemant@prensil.com>
 */
use PHPUnit\Framework\TestCase;

require __DIR__.'/../vendor/autoload.php';
require __DIR__.'/../config.php';
use Services\XeroAccountServices;
use Models\AccountModel;

class XeroAccountImportTest extends TestCase
{
    /**
     * Check if import is successful.
     * Match the count return from API and stored in DB.
     */
    public function testXeroAccountSuccessfulImport()
    {
        global $config;
        $accountService = new XeroAccountServices($config);
        $response = $accountService->saveAccounts();
        if ($response['success']) {
            $total_accounts = count($accountService->accounts);
            $account_obj = AccountModel::where('1');
            $db_cnt = $account_obj->count();
            $this->assertTrue($total_accounts == $db_cnt);
        } else {
            $this->assertTrue(false);
        }
    }

    /**
     * Check if import is successful in case of account is already exist.
     * Account details should be overwritten with the API details.
     */
    public function testExistingAccountUpdate()
    {
        global $config;
        $account = AccountModel::getOne();
        $original_val = $account->currency_code;    //Preserve the value
        $account->currency_code = 'CADD';   // Update any field with dummy values.
        $account->save();

        // Pull account details and store them in DB.
        $accountService = new XeroAccountServices($config);
        $response = $accountService->saveAccounts();

        if ($response['success']) {
            $updated_account = AccountModel::byId($account->account_id);
            // If value is overwritten then it should not match with dummy value.
            $this->assertTrue($updated_account->currency_code != $account->currency_code);
        } else {
            // reset with original value.
            $account->currency_code = $original_val;
            $account->save();
            $this->assertTrue(false);
        }
    }
}
