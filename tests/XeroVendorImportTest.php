<?php
/**
 * XeroVendorImportTest Class.
 * Test cases for vendor import functionality.
 * Check successful insert and update if exist already.
 *
 * @author    Hemant Patel <Hemant@prensil.com>
 */
use PHPUnit\Framework\TestCase;

require __DIR__.'/../vendor/autoload.php';
require __DIR__.'/../config.php';
use Services\XeroVendorServices;
use Models\VendorModel;

class XeroVendorImportTest extends TestCase
{
    /**
     * Check if import is successful.
     * Match the count return from API and stored in DB.
     */
    public function testXeroVendorSuccessfulImport()
    {
        global $config;
        $vendorService = new XeroVendorServices($config);
        $response = $vendorService->saveVendors();
        if ($response['success']) {
            $total_vendors = count($vendorService->vendors);
            $vendor_obj = VendorModel::where('1');
            $db_cnt = $vendor_obj->count();
            $this->assertTrue($total_vendors == $db_cnt);
        } else {
            $this->assertTrue(false);
        }
    }

    /**
     * Check if import is successful in case of vendor is already exist.
     * Vendor details should be overwritten with the API details.
     */
    public function testExistingVendorUpdate()
    {
        global $config;
        $vendor = VendorModel::getOne();
        $original_val = $vendor->skype;    //Preserve the value
        $vendor->skype = 'beanworks';   // Update any field with dummy values.
        $vendor->save();

        // Pull vendor details and store them in DB.
        $vendorService = new XeroVendorServices($config);
        $response = $vendorService->saveVendors();

        if ($response['success']) {
            $updated_vendor = VendorModel::byId($vendor->vendor_id);
            // If value is overwritten then it should not match with dummy value.
            $this->assertTrue($updated_vendor->skype != $vendor->skype);
        } else {
            // reset with original value.
            $vendor->skype = $original_val;
            $vendor->save();
            $this->assertTrue(false);
        }
    }
}
