<?php

use PHPUnit\Framework\TestCase;

require __DIR__.'/../vendor/autoload.php';
require __DIR__.'/../config.php';
use XeroPHP\Application\PrivateApplication;
use XeroPHP\Models\Accounting\Contact;

class XeroConnectionTest extends TestCase
{
    public function testXeroConnection()
    {
        global $config;
        $app = new PrivateApplication($config);
        try {
            $contacts = $app->load(Contact::class)->execute();
            $flag = true;
        } catch (\Exception $e) {
            $flag = false;
        }
        $this->assertTrue($flag);
    }
}
