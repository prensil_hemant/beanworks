<?php
/**
 * XeroInvoiceImportTest Class.
 * Test cases for invoice import functionality.
 * Check successful insert and update if exist already.
 *
 * @author    Hemant Patel <Hemant@prensil.com>
 */
use PHPUnit\Framework\TestCase;

require __DIR__.'/../vendor/autoload.php';
require __DIR__.'/../config.php';
use Services\XeroInvoiceServices;
use Models\InvoiceModel;

class XeroInvoiceImportTest extends TestCase
{
    /**
     * Check if import is successful.
     * Match the count return from API and stored in DB.
     */
    public function testXeroInvoiceSuccessfulImport()
    {
        global $config;
        $invoiceService = new XeroInvoiceServices($config);
        $response = $invoiceService->saveInvoices();
        if ($response['success']) {
            $total_invoices = count($invoiceService->invoices);
            $invoice_obj = InvoiceModel::where('1');
            $db_cnt = $invoice_obj->count();
            $this->assertTrue($total_invoices == $db_cnt);
        } else {
            $this->assertTrue(false);
        }
    }

    /**
     * Check if import is successful in case of invoice is already exist.
     * Invoice details should be overwritten with the API details.
     */
    public function testExistingVendorUpdate()
    {
        global $config;
        $invoice = InvoiceModel::getOne();
        $original_val = $invoice->currency;    //Preserve the value
        $invoice->currency = 'CADD';   // Update any field with dummy values.
        $invoice->save();

        // Pull vendor details and store them in DB.
        $invoiceService = new XeroInvoiceServices($config);
        $response = $invoiceService->saveInvoices();

        if ($response['success']) {
            $updated_invoice = InvoiceModel::byId($invoice->invoice_id);
            // If value is overwritten then it should not match with dummy value.
            $this->assertTrue($updated_invoice->currency != $invoice->currency);
        } else {
            // reset with original value.
            $invoice->currency = $original_val;
            $invoice->save();
            $this->assertTrue(false);
        }
    }
}
