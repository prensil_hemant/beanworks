<?php

use PHPUnit\Framework\TestCase;

class DBConnectionTest extends TestCase
{
    public function testDatabaseConnection()
    {
        $connection = parse_ini_file(__DIR__.'/../database.ini');
        try {
            $link = mysqli_connect($connection['host'], $connection['username'], $connection['password']);
        } catch (Exception $e) {
            $link = false;
        }
        if ($link) {
            $this->assertTrue(true);
        } else {
            $this->assertTrue(false);
        }
    }
}
