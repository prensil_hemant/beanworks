<?php

$config = [
    'app_type' => 'PRIVATE',
    'xero' => [
        // API versions can be overridden if necessary for some reason.
        //'core_version'     => '2.0',
        //'payroll_version'  => '1.0',
        //'file_version'     => '1.0'
    ],
    'oauth' => [
        'callback' => 'http://l034759.corp.ads:8080/beanworks/app/callback.php',
        'consumer_key' => 'STIQLIA48XN7YPCYX82KUHNLQALFDT',
        'consumer_secret' => 'AUEE2JUBCHREQKX0HNQTQ8QJOTYVZH',
        //If you have issues passing the Authorization header, you can set it to append to the query string
        //'signature_location'    => \XeroPHP\Remote\OAuth\Client::SIGN_LOCATION_QUERY
        //For certs on disk or a string - allows anything that is valid with openssl_pkey_get_(private|public)
        'rsa_private_key' => file_get_contents(__DIR__.'/certs/privatekey.pem'),
        'rsa_public_key' => file_get_contents(__DIR__.'/certs/publickey.cer'),
    ],
    //These are raw curl options.  I didn't see the need to obfuscate these through methods
    'curl' => [
        CURLOPT_USERAGENT => 'XeroPHP Test App',
        //Only for partner apps - unfortunately need to be files on disk only.
        //CURLOPT_CAINFO          => file_get_contents('./certs/certificate.crt'),
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
        //CURLOPT_SSLCERT         => 'certs/entrust-cert-RQ3.pem',
        //CURLOPT_SSLKEYPASSWD    => '1234',
        //CURLOPT_SSLKEY          => 'certs/entrust-private-RQ3.pem'
    ],
];
