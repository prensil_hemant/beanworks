<?php
/**
 * AccountModel Class.
 * Model class represents a DB table.
 * It's an ORM wrapper to perform DB operations on accounts table.
 *
 * @author    Hemant Patel <Hemant@prensil.com>
 */

namespace Models;

require_once __DIR__.'/../db/MysqliDb.php';
require_once __DIR__.'/../db/dbObject.php';

class AccountModel extends \dbObject
{
    protected $dbTable = 'accounts';
    protected $primaryKey = 'account_id';
    // DB table fields and their types. Perform some basic validation based on defined types in library class.
    protected $dbFields = array(
        'account_id' => array('varchar', 'required'),
        'name' => array('varchar', 'required'),
        'type' => array('varchar'),
        'description' => array('varchar'),
        'tax_type' => array('varchar'),
        'bank_account_type' => array('varchar'),
        'currency_code' => array('varchar'),
        'reporting_code_name' => array('varchar'),
        'enable_payment_to_account' => array('bool'),
        'show_in_expense_claims' => array('bool'),
        'updated_at' => array('datetime'),
        'status' => array('varchar'),
    );

    /**
     * Process the provided array and create/update account entry in DB.
     *
     * @param array
     *
     * @return int
     */
    public function saveAccount($data = [])
    {
        if (count($data) > 0) {
            $this->account_id = $data['AccountID'];
            $this->name = $data['Name'];
            $this->type = $data['Type'];
            $this->description = $data['Description'];
            $this->tax_type = $data['TaxType'];
            $this->bank_account_type = $data['BankAccountType'];
            $this->currency_code = $data['CurrencyCode'];
            $this->reporting_code_name = $data['ReportingCodeName'];
            $this->enable_payment_to_account = $data['EnablePaymentsToAccount'];
            $this->show_in_expense_claims = $data['ShowInExpenseClaims'];
            $this->updated_at = $data['updated_at'];
            $this->status = $data['Status'];
            // Save method will return the last saved vendor id.
            // If it's null then there is some error and it will be stored in model->errors
            $id = $this->save();
            if ($id == null) {
                return 0;
            } else {
                return $id;
            }
        } else {
            return 0;
        }
    }
}
