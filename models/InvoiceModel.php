<?php
/**
 * InvoiceModel Class.
 * Model class represents a DB table.
 * It's an ORM wrapper to perform DB operations on invoices table.
 *
 * @author    Hemant Patel <Hemant@prensil.com>
 */

namespace Models;

require_once __DIR__.'/../db/MysqliDb.php';
require_once __DIR__.'/../db/dbObject.php';

class InvoiceModel extends \dbObject
{
    protected $dbTable = 'invoices';
    protected $primaryKey = 'invoice_id';
    // DB table fields and their types. Perform some basic validation based on defined types in library class.
    protected $dbFields = array(
        'invoice_id' => array('varchar', 'required'),
        'invoice_no' => array('varchar'),
        'vendor_id' => array('varchar', 'required'),
        'currency' => array('varchar', 'required'),
        'invoice_status' => array('varchar', 'required'),
        'subtotal' => array('double', 'required'),
        'total_tax' => array('double', 'required'),
        'total_amount' => array('double', 'required'),
        'amount_due' => array('double', 'required'),
        'amount_paid' => array('double', 'required'),
        'amount_credited' => array('double', 'required'),
        'fully_paid_date' => array('datetime'),
        'invoice_date' => array('datetime', 'required'),
        'due_date' => array('datetime', 'required'),
    );

    /**
     * Process the provided array and create/update Invoice entry in DB.
     *
     * @param array
     *
     * @return int
     */
    public function saveInvoice($data = [])
    {
        if (count($data) > 0) {
            $this->invoice_id = $data['InvoiceID'];
            $this->invoice_no = $data['InvoiceNumber'];
            $this->vendor_id = $data['VendorID'];
            $this->currency = $data['CurrencyCode'];
            $this->invoice_status = $data['Status'];
            $this->subtotal = $data['SubTotal'];
            $this->total_tax = $data['TotalTax'];
            $this->total_amount = $data['Total'];
            $this->amount_due = $data['AmountDue'];
            $this->amount_paid = $data['AmountPaid'];
            $this->amount_credited = $data['AmountCredited'];
            $this->fully_paid_date = $data['FullyPaidOnDate'];
            $this->invoice_date = $data['invoice_date'];
            $this->due_date = $data['due_date'];
            // Save method will return the last saved vendor id.
            // If it's null then there is some error and it will be stored in model->errors
            $id = $this->save();
            if ($id == null) {
                return 0;
            } else {
                return $id;
            }
        } else {
            return 0;
        }
    }
}
