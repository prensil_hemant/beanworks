<?php
/**
 * VendorModel Class.
 * Model class represents a DB table.
 * It's an ORM wrapper to perform DB operations on vendors table.
 *
 * @author    Hemant Patel <Hemant@prensil.com>
 */

namespace Models;

require_once __DIR__.'/../db/MysqliDb.php';
require_once __DIR__.'/../db/dbObject.php';

class VendorModel extends \dbObject
{
    protected $dbTable = 'vendors';
    protected $primaryKey = 'vendor_id';
    // DB table fields and their types. Perform some basic validation based on defined types in library class.
    protected $dbFields = array(
        'vendor_id' => array('varchar', 'required'),
        'name' => array('varchar', 'required'),
        'tax_number' => array('varchar'),
        'first_name' => array('varchar'),
        'last_name' => array('varchar'),
        'email' => array('varchar'),
        'skype' => array('varchar'),
        'is_supplier' => array('bool'),
        'is_customer' => array('bool'),
        'network_key' => array('varchar'),
        'website' => array('varchar'),
        'status' => array('varchar'),
    );

    /**
     * Process the provided array and create/update Vendor entry in DB.
     *
     * @param array
     *
     * @return int
     */
    public function saveVendor($data = [])
    {
        if (count($data) > 0) {
            $this->vendor_id = $data['VendorID'];
            $this->name = $data['Name'];
            $this->tax_number = $data['TaxNumber'];
            $this->first_name = $data['FirstName'];
            $this->last_name = $data['LastName'];
            $this->email = $data['EmailAddress'];
            $this->skype = $data['SkypeUserName'];
            $this->is_supplier = $data['IsSupplier'];
            $this->is_customer = $data['IsCustomer'];
            $this->network_key = $data['XeroNetworkKey'];
            $this->website = $data['Website'];
            $this->status = $data['Status'];
            // Save method will return the last saved vendor id.
            // If it's null then there is some error and it will be stored in model->errors
            $id = $this->save();
            if ($id == null) {
                return 0;
            } else {
                return $id;
            }
        } else {
            return 0;
        }
    }
}
