import React,{Component} from 'react';
import axios from 'axios';
import $ from 'jquery';
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/styles.css';

import ReactTable from "react-table";
import 'react-table/react-table.css';
const account_list_url = '../controllers/pull-accounts.php';
const account_fetch_url = '../controllers/fetch-accounts.php';

const requestData = (pageSize, page, sorted, filtered,url) => {
return new Promise((resolve, reject) => {
    // You can retrieve your data however you want, in this case, we will just use some local data.
    axios.post(url,{
        'pageSize':pageSize,
        'pageNo':page+1,
        'filters':filtered,
        'sort_params':sorted
    }).then(response => { 
            if(response.status === 200){
                // You must return an object containing the rows of the current page, and optionally the total pages number.
                let res = {rows:[],pages:0};
                if(response.data.resultData){
                    res = {
                        rows: response.data.resultData.searchResults,
                        pages: Math.ceil(response.data.resultData.totalAvailable / pageSize)
                    };
                }
                resolve(res);
            }else{
                console.log(`Error :${response.status}`);
            }
        },(error) => { console.log(error) }
        );
    });
};

class AccountsList extends Component{
    constructor() {
        super();
        this.state = {
          data: [],
          pages: null,
          loading: true
        };
        this.fetchData = this.fetchData.bind(this);
        this.refreshData = this.refreshData.bind(this);
    }
    refreshData(){
        return new Promise((resolve, reject) => {
        // You can retrieve your data however you want, in this case, we will just use some local data.
        axios.post(account_fetch_url,{
        }).then(response => { 
                if(response.status === 200){
                    if(response.data.success){
                        $('#response_txt').text("Successfully imported accounts.");
                        $('#notifications').removeClass('d-none');
                        $('#notifications').removeClass('alert alert-danger');
                        $('#notifications').addClass('alert alert-success');
                        this.fetchData(this.state);
                    }else{
                        //document.getElementById("response_txt").innerText(response.data.errors);
                        $('#response_txt').text(JSON.stringify(response.data.errors));
                        $('#notifications').removeClass('d-none');
                        $('#notifications').removeClass('alert alert-success');
                        $('#notifications').addClass('alert alert-danger');
                    }
                    $(window).scrollTop(0);
                    $('#notifications').show().fadeOut(5000);
                }else{
                    console.log(`Error :${response.status}`);
                    return false;
                }
            },(error) => { console.log(error);return false; }
            );
        });
    }
    fetchData(state, instance) {
    // Whenever the table model changes, or the user sorts or changes pages, this method gets called and passed the current table model.
    // You can set the `loading` prop of the table to true to use the built-in one or show you're own loading bar if you want.
    this.setState({ loading: true });
    // Request the data however you want.  Here, we'll use our mocked service we created earlier
    requestData(
        state.pageSize,
        state.page,
        state.sorted,
        state.filtered,
        account_list_url
    ).then(res => {
        // Now just get the rows of data to your React Table (and update anything else like total pages or loading)
        this.setState({
        data: res.rows,
        pages: res.pages,
        loading: false
        });
    });
    }
    render(){
        const { data, pages, loading } = this.state;
        return (
            <div className="Accounts">
                <button type="button" className="btn btn-primary float-right bottom20 clearfix" onClick={this.refreshData}>Refresh</button>
                <ReactTable
                    columns={[
                        {
                            Header: "Account ID",
                            accessor: "account_id"
                        },
                        {
                            Header: "Name",
                            accessor: "name"
                        },
                        {
                            Header: "Description",
                            accessor: "description"
                        },
                        {
                            Header: "Type",
                            accessor: "type"
                        },
                        {
                            Header: "Tax Type",
                            accessor: "tax_type"
                        },
                        {
                            Header: "Reporting Code",
                            accessor: "reporting_code_name"
                        },
                        {
                            Header: "Payment Enabled",
                            accessor: "enable_payment_to_account"
                        },
                        {
                            Header: "Show in expense Claim",
                            accessor: "show_in_expense_claims"
                        },
                        {
                            Header: "Updated At",
                            accessor: "updated_at"
                        },
                    ]}
                    manual // Forces table not to paginate or sort automatically, so we can handle it server-side
                    data={data}
                    pages={pages} // Display the total number of pages
                    loading={loading} // Display the loading overlay when we need it
                    onFetchData={this.fetchData} // Request new data when things change
                    filterable
                    defaultPageSize={10}
                    className="-striped -highlight"
                />
            </div>
        );
    }
}

export default AccountsList;