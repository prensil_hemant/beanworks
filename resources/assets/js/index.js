import React from 'react';
import ReactDOM from 'react-dom';
import VendorsList from './vendors_list';
import InvoicesList from './invoices_list';
import AccountsList from './accounts_list';

ReactDOM.render(<VendorsList/>,document.getElementById('vendors'));
ReactDOM.render(<InvoicesList/>,document.getElementById('invoices'));
ReactDOM.render(<AccountsList/>,document.getElementById('accounts'));